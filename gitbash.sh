#! /bin/bash

str="major,bug" # replace with CI_MERGE_REQUEST_LABELS
IFS=',' # , is set as delimiter
read -ra allLabel <<< "$str" # str is read into an array as tokens separated by IFS

check_log(){
    for i in "${allLabel[@]}"; do # access each element of array
        if [ $i == $1 ];
        then 
            return 0
        fi
    done
    return 1
}

updateVersion(){
    versionArray[$1]=$((versionArray[$1]+1))
    var=$( IFS=.; echo "${versionArray[*]}" )
    git tag "$var"
    git tag
    git push --tags
}

if  check_log "improvement";
    then
        updateVersion 0
elif check_log "major"
    then 
        updateVersion 1
elif check_log "bug"
    then
        updateVersion 2
fi